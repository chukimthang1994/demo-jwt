class AuthenticationController < ApplicationController
  skip_before_action :authorize_request

  # POST /auth/login
  def login
    user = User.find_by(email: params[:email])

    if user&.authenticate(params[:password])
      token = encode(user_id: user.id)
      render json: { token: token }, status: :ok
    else
      render json: { errors: 'unauthorized'}, status: :unauthorized
    end
  end
end
